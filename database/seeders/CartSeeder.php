<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Cart;
use App\Models\Product;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $products = Product::inRandomOrder()->limit(3)->get();
        $cart = Cart::factory(3)->hasAttached(
            $products,
            function () {
                return ['quantity' => rand(1, 100)];
            }
        )
            ->create();
    }
}
