<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->catchPhrase(),
            'price' => rand(10, 1000),
            'quantity' => rand(1, 100),
            'description' => fake()->realText($maxNbChars = 150, $indexSize = 2),
            'category_id' => Category::inRandomOrder()->first()->id,
        ];
    }
}
